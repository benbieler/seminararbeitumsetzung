# Seminararbeit - Bildgrößenänderungen von Benjamin Bieler

## Introduction

>Der Begriff „Skalieren“ wird häufig in den verschiedensten Kontexten verwendet.
„Skalieren“ stammt ursprünglich aus dem italienischen („scalae“) und bedeutet so viel wie „Treppe“ was einem schon einen ganz guten Einblick in die Bedeutung gibt. In der Mathematik versteht man unter „Skalieren“ die Multiplikation mit einer Zahl (Skalarmultiplikation). In der Betriebswirtschaftslehre lernt man, wie man die Wachstumsfähigkeit eines Geschäftsmodells skaliert.
Für diese Seminararbeit ist die Größenänderung von digitalen Bildern relevant. 
Jeden Tag skaliert man Bilder - bewusst oder unbewusst. Lädt man ein neues Profilbild hoch, öffnet eine Webseite mit einem Bild, verwendet Bildbetrachter oder verschickt eine Aufnahme über WhatsApp wird dieses Foto in den meisten Fällen entsprechend der verlangten Maße hoch oder runterskaliert.
Bewusst verändert man die Größe von Bildern in gängigen Bildbearbeitungsprogrammen wie etwa Photoshop, Gimp oder Photoscape.
Hier stehen einem zahlreiche Skalierungsmethoden zur Verfügung, die am häufigsten verwendete Verfahren sind: Nearest Neighbor Interpolation, Linear Interpolation, Cubic Interpolation. 
Nun ist es nicht jedem bewusst, wie viel Mathematik gebraucht wird, um eine Rastergraphik zu skalieren, wenn man gleichzeitig eine akzeptable Auflösung beibehalten möchte. 
In der folgenden Arbeit wird dem Leser der mathematische Hintergrund nahegebracht.
Anschließend soll anhand eines selbst gewählten Beispiels erklärt werden wie derartige Algorithmen in der Softwareentwicklung eingesetzt werden. Es wird folglich ein gutes Programmierverständnis vorausgesetzt (bestenfalls in Java). Eine Anleitung zur Installation der notwendigen Hilfsmittel findet sich im Anhang.

## Code Samples


        ImageManipulatorController imageManipulatorController = new ImageManipulatorController(imageManipulatorModel, calculatorView);

        calculatorView.setVisible(true);
    

## Installation

1. Clone repo: 
2. Run the .exe