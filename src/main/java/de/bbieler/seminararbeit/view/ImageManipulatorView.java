/*
 * (c) 2017 Benjamin Bieler <ben@benbieler.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

package de.bbieler.seminararbeit.view;

import de.bbieler.seminararbeit.model.ImageManipulatorModel;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Logger;

/**
 * main.java.de.bbieler.seminararbeit.view.ImageManipulatorView
 *
 * @author benbieler <15/04/2017>.
 */
public class ImageManipulatorView extends JFrame {

    private String[] interpolationAlgorithms = {"Nearest Neighbor Interpolation", "Linear Interpolation", "Cubic Spline Interpolation"};
    private final static Logger LOGGER = Logger.getLogger(ImageManipulatorModel.class.getName());

    private JTextField firstNumber = new JTextField("120", 5);
    private JTextField secondNumber = new JTextField("145", 5);
    private JTextField thirdNumber = new JTextField("130", 5);
    private JLabel scalingSymbol = new JLabel(" hochskalieren um den Faktor:");
    private JTextField scalingFactor = new JTextField("3", 5);
    private JButton addButton = new JButton("Ausrechnen");
    private JButton clearButton = new JButton("Zurücksetzen");
    private JComboBox options = new JComboBox(interpolationAlgorithms);

    String[][] content = {{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},
                          {"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},
                          {"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""},{"", ""}};
    String[] titel = {"x-Wert", "Grauwert"};

    private JTable table = new JTable(content, titel);

    public ImageManipulatorView() {
        JPanel jPanel = new JPanel();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Seminararbeit (Ben Bieler): Bildgrößenänderungen");
        this.setResizable(false);
        this.setIconImage(new ImageIcon("../util/icon.png").getImage());

        this.setSize(1100, 550);

        jPanel.add(firstNumber);
        jPanel.add(secondNumber);
        jPanel.add(thirdNumber);
        jPanel.add(scalingSymbol);
        jPanel.add(scalingFactor);
        jPanel.add(options);
        jPanel.add(addButton);
        jPanel.add(clearButton);

        jPanel.add(new JScrollPane(table));

        this.add(jPanel);
    }

    public int getFirstGS() {
        return Integer.parseInt(this.firstNumber.getText());
    }

    public int getSecondGS() {
        return Integer.parseInt(this.secondNumber.getText());
    }
    public int getThirdGS() {
        return Integer.parseInt(this.thirdNumber.getText());
    }

    public int getScalingFactor() {
        return Integer.parseInt(this.scalingFactor.getText());
    }

    public String getInterpolationType() {
        return interpolationAlgorithms[this.options.getSelectedIndex()];
    }

    public void setNNICalculationResult(List<Float> solution) {
        for (int i=0; i<solution.size(); i++) {
            table.setValueAt("" + i, i, 0);
            table.setValueAt("" + solution.get(i), i, 1);
        }
    }

    public void setLICalculationResult(List<Float> solution) {
        for (int i=0; i<solution.size(); i++) {
            table.setValueAt("" + i, i, 0);
            table.setValueAt("" + solution.get(i), i, 1);
        }
    }

    public void setCUCalculationResult(List<Float> solution) {
        for (int i=0; i<solution.size(); i++) {
            table.setValueAt("" + i, i, 0);
            table.setValueAt("" + solution.get(i), i, 1);
        }
    }

    public void clearTable() {
        for (int i=0; i<content.length; i++) {
            table.setValueAt("", i, 0);
            table.setValueAt("", i, 1);
        }
    }

    public void addCalculationListener(ActionListener actionListener) {
        addButton.addActionListener(actionListener);
    }
    public void addClearListener(ActionListener actionListener) {
        clearButton.addActionListener(actionListener);
    }

    public void displayErrorMessage(String errorMessage) {
        JOptionPane.showMessageDialog(this, errorMessage);
    }
}
