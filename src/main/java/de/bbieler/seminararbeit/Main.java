/*
 * (c) 2017 Benjamin Bieler <ben@benbieler.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

package de.bbieler.seminararbeit;

import de.bbieler.seminararbeit.controller.ImageManipulatorController;
import de.bbieler.seminararbeit.model.ImageManipulatorModel;
import de.bbieler.seminararbeit.view.ImageManipulatorView;

/**
 * Main entry point of the program
 *
 * @author Benjamin Bieler <ben@benbieler.com>
 */
public class Main {
    public static void main(String[] args) {
        ImageManipulatorView calculatorView = new ImageManipulatorView();
        ImageManipulatorModel imageManipulatorModel = new ImageManipulatorModel();

        ImageManipulatorController imageManipulatorController = new ImageManipulatorController(imageManipulatorModel, calculatorView);

        calculatorView.setVisible(true);
    }
}
