/*
 * (c) 2017 Benjamin Bieler <ben@benbieler.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

package de.bbieler.seminararbeit.model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * main.java.de.bbieler.seminararbeit.model.ImageManipulatorModel
 *
 * @author Benjamin Bieler <ben@benbieler.com> <20/10/2017>.
 */
public class ImageManipulatorModel {

    private final static Logger LOGGER = Logger.getLogger(ImageManipulatorModel.class.getName());

    /**
     * Nearest neighbor interpolation algorithm
     *
     * @param fGs first Grey scale value
     * @param sGs second Grey scale value
     * @param sF  scaling factor
     * @return List<Integer>
     */
    public List<Float> handleNNICalculation(float fGs, float sGs, float tGs, float sF) {
        float x1[] = {0, sF, 2 * sF};
        float y1[] = {fGs, sGs, tGs};

        List<Float> newGsValues = new ArrayList<>();

        for (int j = 0; j <= x1[2]; j++) {
            newGsValues.add(j, (float) nearestNeighborInterpolatonImpl(x1, y1, j));
        }

        return newGsValues;
    }


    /**
     * Linear interpolation algorithm
     *
     * @param fGs first Grey scale value
     * @param sGs second Grey scale value
     * @param sF  scaling factor
     * @param tGs third Grey scale value
     * @return List<Integer>
     */
    public List<Float> handleLICalculation(float fGs, float sGs, float tGs, float sF) {
        // Aufgabe: geg: x1 = 0; fx1=fGs; Xu=i; x2=sf; fx2=sGs; | ges: fXu;
        // Liste der neuen Gs Werte:

        float x1[] = {0, sF, 2 * sF};
        float y1[] = {fGs, sGs, tGs};

        List<Float> newGsValues = new ArrayList<>();

        for (int j = 0; j <= x1[2]; j++) {
            newGsValues.add(j, (float) linearInterpolatonImpl(x1, y1, j));
        }

        return newGsValues;

    }


    /**
     * Cubic interpolation algorithm
     *
     * @param fGs first Grey scale value
     * @param sGs second Grey scale value
     * @param sF  scaling factor
     * @return List<Integer>
     */
    public List<Float> handleCUCalculation(float fGs, float sGs, float tGs, float sF) {
        double x1[] = {0, sF, 2 * sF};
        double y1[] = {fGs, sGs, tGs};

        List<Float> newGsValues = new ArrayList<>();

        for (int j = 0; j <= x1[2]; j++) {
            newGsValues.add(j, (float) splineInterpolationImpl(x1, y1, j));
        }

        return newGsValues;
    }

    private static double nearestNeighborInterpolatonImpl(float[] dataX, float[] dataY, int x) {
        float firstMiddle = dataX[1] / 2;
        float secondMiddle = dataX[2] - firstMiddle;

        if (x <= firstMiddle) {
            return dataY[0];
        }

        if (x > firstMiddle && x <= secondMiddle) {
            return dataY[1];
        }

        return dataY[2];
    }

    private static double linearInterpolatonImpl(float[] dataX, float[] dataY, float x) {
        if (x < dataX[1]) {
            float division =  (x - dataX[0]) / (dataX[1] - dataX[0]);

            float factor = dataY[1] - dataY[0];

            return dataY[0] + (division * factor);
        } else {
            float division = (x - dataX[1]) / (dataX[2] - dataX[1]);

            float factor =  dataY[2] - dataY[1];

            return dataY[1] + (division * factor);
        }
    }

    /**
     * Gauss algorithm to solve the matrix
     *
     * @param matrix double[][] from splineInterpolationImpl
     * @return double[]
     */
    private static double[] gauss(double[][] matrix) {
        double[] results = new double[matrix.length];
        int[] order = new int[matrix.length];

        for (int i = 0; i < order.length; ++i) {
            order[i] = i;
        }

        for (int i = 0; i < matrix.length; ++i) {
            int maxIndex = i;
            for (int j = i + 1; j < matrix.length; ++j) {
                if (Math.abs(matrix[maxIndex][i]) < Math.abs(matrix[j][i])) {
                    maxIndex = j;
                }
            }

            if (maxIndex != i) {

                {
                    int temp = order[i];
                    order[i] = order[maxIndex];
                    order[maxIndex] = temp;
                }

                for (int j = 0; j < matrix[0].length; ++j) {
                    double temp = matrix[i][j];
                    matrix[i][j] = matrix[maxIndex][j];
                    matrix[maxIndex][j] = temp;
                }
            }

            if (Math.abs(matrix[i][i]) < 1e-15) {
                throw new RuntimeException("Singularity detected");
            }

            for (int j = i + 1; j < matrix.length; ++j) {

                double factor = matrix[j][i] / matrix[i][i];

                for (int k = i; k < matrix[0].length; ++k) {
                    matrix[j][k] -= matrix[i][k] * factor;
                }
            }
        }

        for (int i = matrix.length - 1; i >= 0; --i) {

            results[i] = matrix[i][matrix.length];
            for (int j = i + 1; j < matrix.length; ++j) {
                results[i] -= results[j] * matrix[i][j];
            }
            results[i] /= matrix[i][i];
        }

        double[] correctResults = new double[results.length];

        for (int i = 0; i < order.length; ++i) {
            correctResults[order[i]] = results[i];
        }

        return results;
    }

    /**
     * Spline Interpolation
     *
     * @param dataX x coordinates
     * @param dataY y coordinates
     * @param x     x value to interpolate
     * @return double
     */
    private static double splineInterpolationImpl(double[] dataX, double[] dataY, double x) {
        int xIndex = 0;
        int power = 2;

        while (xIndex < dataX.length - (1 + power + (dataX.length - 1) % power) && dataX[xIndex + power] < x) {
            xIndex += power;
        }

        double matrix[][] = new double[power + 1][power + 2];

        for (int i = 0; i < power + 1; ++i) {
            for (int j = 0; j < power; ++j) {
                matrix[i][j] = Math.pow(dataX[xIndex + i], (power - j));
            }

            matrix[i][power] = 1;
            matrix[i][power + 1] = dataY[xIndex + i];
        }

        double[] coefficients = gauss(matrix);
        double answer = 0;

        for (int i = 0; i < coefficients.length; ++i) {
            answer += coefficients[i] * Math.pow(x, (power - i));
        }

        return answer;
    }
}
